import React, { Component } from "react";
import {RangeSlider} from "reactrangeslider";
import {sliderRange} from '../components/redux/actions/SliderAction'
import {connect} from 'react-redux'

class RangeSliders extends Component {

    constructor(props, context) {
        super(props, context);
        
        this.state = {
            start: 0,
            end: this.props.videoDuration > 0 ? this.props.videoDuration : 100
        }
        this.onChange = this.onChange.bind(this)
        
        };

    onChange(e) {
        console.log(e,"nnn")
        this.setState({
            start: e.start,
            end: e.end
        })
    }    
  render() {
      var value = {
          start: this.state.start,
          end: this.state.end
      }
    return (
      <div>
        <RangeSlider
          value={ value }
          onChange={ this.onChange }
          min={0}
          max={this.props.videoDuration > 0 ? this.props.videoDuration : 100}
          step={1}
          
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
    return{
        range: state.start
    }
}

const mapDispatchToProps = dispatch => {
    return{
        sliderRange: () => dispatch(sliderRange(12))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RangeSliders)