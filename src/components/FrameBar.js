import React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import "./videoStyle.scss";
import "./video.css";

function FrameBar(props) {
  return (
    <div>
      <div>
        {!props.openCustomFrames ? (
          <div
            style={{
              width: "1280px",
              overflow: "scroll",
              whiteSpace: "nowrap",
              marginLeft: "80px",
              maxWidth: "100%",
              display: "flex",
              position: "relative",
            }}
          >
            {/* progress line div */}
            <div
              className="progressLine"
              style={{
                border: "1px solid black",
                width: " 2px",
                height: "48px",
                background: "white",
                position: "absolute",
                left: `${props.moveProgressLine}px`,
                zIndex: "999",
                paddingTop: "2px",
              }}
            ></div>

            {/* progress line div ends */}

            {props.framesLine.length > 0
              ? props.windowFrames.length > 0
                ? props.windowFrames.map((item, index) => (
                    // frames for window 1 & 2
                    <img
                      src={item.content}
                      key={index}
                      alt="customframes"
                      className={`${
                        props.window1Start === null ||
                        props.window1End === null ||
                        props.window1Start > index ||
                        props.window1End < index
                          ? props.window2Start === null ||
                            props.window2End === null ||
                            props.window2Start > index ||
                            props.window2End < index
                            ? "normal"
                            : "selectedWinTwo"
                          : "selectedWinOne"
                      }`}
                      style={{
                        width: `${props.windowFrameImgSize}px`,
                        // height: `${windowFrameImgSize}px`,
                        height: "50px",
                      }}
                      onClick={() => props.showPreviewOnClick(item.content)}
                    />
                  ))
                : props.framesLine.map((item, index) => (
                    // frames for normal full video
                    <img
                      src={item.content}
                      key={index}
                      alt="customframes"
                      className={`${
                        props.window1Start === null ||
                        props.window1End === null ||
                        props.window1Start > index ||
                        props.window1End < index
                          ? props.window2Start === null ||
                            props.window2End === null ||
                            props.window2Start > index ||
                            props.window2End < index
                            ? "normal"
                            : "selectedWinTwo"
                          : "selectedWinOne"
                      }`}
                      style={{
                        width: `${props.normalFrameImgSize}px`,
                        // height: `${normalFrameImgSize}px`,
                        height: "50px",
                      }}
                      onClick={() => props.showPreviewOnClick(item.content)}
                    />
                  ))
              : ""}
          </div>
        ) : (
          // frames for custom playback
          <DragDropContext onDragEnd={props.onDragEnd}>
            <Droppable droppableId="droppable" direction="horizontal">
              {(provided, snapshot) => (
                <div
                  className="customFramesDiv"
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                  style={{
                    width: "1280px",
                    overflow: "scroll",
                    whiteSpace: "nowrap",
                    marginLeft: "80px",
                    maxWidth: "100%",
                    display: "flex",
                  }}
                >
                  {/* progress line div */}
                  <div
                    className="progressLine"
                    style={{
                      border: "1px solid black",
                      width: " 2px",
                      height: "48px",
                      background: "white",
                      position: "absolute",
                      left: `${props.moveProgressLine}px`,
                      zIndex: "999",
                      paddingTop: "2px",
                    }}
                  ></div>

                  {/* progress line div ends */}

                  {props.customFramesExtracted.length > 0
                    ? props.customFramesExtracted.map((item, index) => (
                        <>
                          {item.id === "start" || item.id === "end" ? (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {(provided, snapshot) => (
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                >
                                  {item.content}
                                </div>
                              )}
                            </Draggable>
                          ) : (
                            <Draggable
                              isDragDisabled={true}
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {(provided, snapshot) => (
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                >
                                  <img
                                    src={item.content}
                                    key={index}
                                    alt="customframes"
                                    className={
                                      props.start === null ||
                                      props.end === null ||
                                      props.start > index ||
                                      props.end < index
                                        ? "normal"
                                        : "selected"
                                    }
                                    style={{
                                      width: `${props.customFrameImgSize}px`,
                                      // height: `${customFrameImgSize}px`,
                                      height: "50px",
                                    }}
                                    onClick={() =>
                                      props.showPreviewOnClick(index)
                                    }
                                  />
                                </div>
                              )}
                            </Draggable>
                          )}
                        </>
                      ))
                    : ""}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        )}
      </div>
    </div>
  );
}

export default FrameBar;
