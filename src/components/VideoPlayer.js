import React, { Component } from "react";
import videoToShow from "./sampleVideo.mp4";
import "./videoStyle.scss";
import VideoLooper from "./LoopVideo";
import "./video.css";
import Loader from "./Loader";
import FrameBar from "./FrameBar";
import PlaybackControls from "./PlaybackControls";
// import $ from "jquery";

// const videoToShow =
//   "https://drive.google.com/file/d/1LO8VgkC-L4UItiYvQ8c9Kg4nM3AlPxRj/view";
var framesLine = [];
var lengthOfWinArray;
var moveProgressLine;
var customFrameslength;
var fps = 2;
var startDiv = (
  <div
    style={{
      border: "1px solid red",
      width: " 2px",
      height: "48px",
      background: "red",
    }}
  ></div>
);

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export default class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prevNoOfWinFrames: 0,
      windowTwoClicked: false,
      noOfWinFrames: 0,
      framesExtracted: [],
      framePreviewSrc: [],
      frameIndex: 0,
      showForBackButton: false,
      frameByFrameView: false,
      startTime: 0,
      endTime: 0,
      videoDuration: 0,
      openCustomFrames: false,
      customFramesExtracted: [],
      start: null,
      end: null,
      customStart: 0,
      customEnd: 0,
      framesLine: [],
      window1Start: null,
      window1End: null,
      window2Start: null,
      window2End: null,
      loopFullVideo: false,
      openPlaybackLoopControls: false,
      windowFrames: [],
      currTimeFromLoopComponent: 0,
      videoStatus: "",
    };

    this.fetchCurrTime = this.fetchCurrTime.bind(this);
    this.fullVideoClicked = this.fullVideoClicked.bind(this);
    this.customButtonOnClick = this.customButtonOnClick.bind(this);
    this.passPropsToVideoLooperWinTwo = this.passPropsToVideoLooperWinTwo.bind(
      this
    );
    this.passPropsToVideoLooperWinOne = this.passPropsToVideoLooperWinOne.bind(
      this
    );
    this.onDragEnd = this.onDragEnd.bind(this);
    this.frameByFrameView = this.frameByFrameView.bind(this);
    this.extractFramesFromVideo = this.extractFramesFromVideo.bind(this);
    this.framePreviewForward = this.framePreviewForward.bind(this);
    this.showFrameimage = this.showFrameimage.bind(this);
    this.framePreviewBackward = this.framePreviewBackward.bind(this);
    this.showPreviewOnClick = this.showPreviewOnClick.bind(this);
    this.sliderChange = this.sliderChange.bind(this);
    this.videoPlay = this.videoPlay.bind(this);
    this.videoPause = this.videoPause.bind(this);
  }

  componentDidMount() {
    this.extractFramesFromVideo();
    this.fullVideoClicked();
  }

  componentDidUpdate() {
    if(document.getElementById("my-video")){
      const video = document.getElementById("my-video");
      video.addEventListener("timeupdate", this.checkCurrTime);
      video.addEventListener("play", this.videoPlay);
      video.addEventListener("pause", this.videoPause);
    }
  }

  //this is not required currently but it automatically scrolls the div of frame bar to centre where we have selectors for custom playback onClick of custom button

  // componentDidUpdate() {
  //   if (this.state.customStart == 0 && this.state.customEnd == 0) {
  //     if (document.querySelector(".customFramesDiv")) {
  //       var leftPos = $(".customFramesDiv").scrollLeft();
  //       $("div.customFramesDiv").animate(
  //         {
  //           scrollLeft: leftPos + 900,
  //         },
  //         200
  //       );
  //     }
  //   }
  // }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }
    if (this.state.customFramesExtracted[result.source.index].id === "start") {
      if (result.destination.index < this.state.end - 1) {
        const items = reorder(
          this.state.customFramesExtracted,
          result.source.index,
          result.destination.index
        );

        this.setState({
          customFramesExtracted: items,
          start: result.destination.index,
          customStart: result.destination.index,
          startTime: (result.destination.index + 1) / fps, //+1 bcoz index start from 0
        });
      }
    }
    if (this.state.customFramesExtracted[result.source.index].id === "end") {
      if (result.destination.index > this.state.start + 1) {
        const items = reorder(
          this.state.customFramesExtracted,
          result.source.index,
          result.destination.index
        );

        this.setState({
          end: result.destination.index,
          customEnd: result.destination.index,
          customFramesExtracted: items,
          endTime: (result.destination.index - 1) / fps,
        });
      }
    }
  }

  videoPlay() {
    this.setState({
      videoStatus: "play",
    });
  }

  videoPause() {
    this.setState({
      videoStatus: "pause",
    });
  }

  playVideo() {
    const video = document.getElementById("my-video");
    video.play();
  }

  pauseVideo() {
    const video = document.getElementById("my-video");
    video.pause();
  }

  skipVideo(value) {
    const video = document.getElementById("my-video");

    var currTime = video.currentTime + value;
    if (currTime < video.duration) {
      video.currentTime = currTime;
    } else if (currTime >= video.duration) {
      video.currentTime = video.duration;
    }
  }

  sliderChange(value) {
    const video = document.getElementById("my-video");
    if (value === 100) {
      video.playbackRate = 1;
    } else if (value === 80) {
      video.playbackRate = 1 / 2;
    } else if (value === 60) {
      video.playbackRate = 1 / 3;
    } else if (value === 40) {
      video.playbackRate = 1 / 4;
    } else if (value === 20) {
      video.playbackRate = 1 / 5;
    } else if (value === 0) {
      video.playbackRate = 1 / 10;
    }
  }

  async extractFramesFromVideo() {
    return new Promise(async (resolve) => {
      this.setState({
        showForBackButton: true,
      });
      var me = this;
      if (document.getElementById("my-video")) {
        const video = document.getElementById("my-video");
        let seekResolve;
        video.addEventListener("seeked", async function () {
          if (seekResolve) seekResolve();
        });

        video.addEventListener("loadeddata", async function () {
          me.setState({
            videoDuration: video.duration,
          });
          let duration = video.duration;
          let canvas = document.createElement("canvas");
          let context = canvas.getContext("2d");
          let [w, h] = [video.videoWidth, video.videoHeight];
          canvas.width = w;
          canvas.height = h;
          let frames = [];
          let customFrames = [];
          let interval = 1 / fps;
          let currentTime = 0;

          while (currentTime < duration) {
            video.currentTime = currentTime;
            await new Promise((r) => (seekResolve = r));

            context.drawImage(video, 0, 0, w, h);
            let base64ImageData = canvas.toDataURL();
            let item = {
              content: base64ImageData,
              id: `${currentTime + 1}`,
            };
            customFrames.push(item);
            frames.push(base64ImageData);
            framesLine.push(item);
            currentTime += interval;
            // video.playbackRate = 16;
            me.setState({
              framesExtracted: frames,
              customFramesExtracted: customFrames,
              framesLine: framesLine,
            });
          }
          customFrameslength = me.state.customFramesExtracted.length;
          customFrames.splice(customFrameslength / 2, 0, {
            content: startDiv,
            id: "start",
          });
          customFrames.splice(customFrameslength / 2 + 3, 0, {
            content: startDiv,
            id: `end`,
          });
          resolve(frames);
        });

        // set video src *after* listening to events in case it loads so fast
        // that the events occur before we were listening.
        video.src = videoToShow;
      }
    });
  }

  showPreviewOnClick(e) {
    if (this.state.frameByFrameView) {
      this.setState({
        framePreviewSrc: e,
      });
    }
  }

  framePreviewForward() {
    this.setState((prevState) => {
      return { frameIndex: prevState.frameIndex + 1 };
    }, this.showFrameimage);
  }

  framePreviewBackward() {
    this.setState({
      frameIndex: this.state.frameIndex !== 0 ? this.state.frameIndex - 1 : 0,
    });
    this.showFrameimage();
  }

  showFrameimage() {
    this.setState({
      framePreviewSrc: this.state.framesExtracted[this.state.frameIndex],
    });
  }

  frameByFrameView() {
    this.setState({
      frameByFrameView: !this.state.frameByFrameView,
    });
  }

  passPropsToVideoLooperWinOne(start, end) {
    var windowOneFrames = this.state.framesLine.slice(0, 4); // 0,4,8,14 these are decided by looking in the index of this.state.frameLine bcoz each second has 2 frames if fps=2 and currently it is done manually, will change it dynamically later
    var windowTwoFrames = this.state.framesLine.slice(8, 14);
    var finalArray = windowOneFrames.concat(windowTwoFrames);
    lengthOfWinArray = finalArray.length;

    this.setState({
      windowTwoClicked: false,
      windowFrames: finalArray,
      startTime: start,
      endTime: end,
      openCustomFrames: false,
      loopFullVideo: false,
      openPlaybackLoopControls: true,
      window1Start: 0,
      window1End: 2 * fps - 1, // start time - end time * fps and -1 bcoz array starts from 0 index
      window2Start: 2 * fps - 1 + 1, // it will start from the next index where window1End ends
      window2End: finalArray.length,
      noOfWinFrames: (end - start) * fps, // +1 bcoz array start from 0
    });
  }

  passPropsToVideoLooperWinTwo(start, end) {
    this.passPropsToVideoLooperWinOne(start, end);
    this.setState({
      windowTwoClicked: true,
    });

    this.setState((prevState) => {
      return {
        prevNoOfWinFrames: prevState.noOfWinFrames,
      };
    });
  }

  customButtonOnClick() {
    this.setState({
      windowFrames: [],
      openCustomFrames: true,
      loopFullVideo: false,
      openPlaybackLoopControls: true,
      start:
        this.state.customStart === 0
          ? customFrameslength / 2 // divided by 2 for showing start and end selectors in middle of frame bar
          : this.state.customStart,
      end:
        this.state.customEnd === 0
          ? customFrameslength / 2 + 3 // this +3  can be any number it shows that when user clicks on custom then initially gap of position of selectors is of 3 sec btw start & end selectors. //it is first set in extractFramesFromVideo() method
          : this.state.customEnd,
      startTime:
        this.state.startTime === 0
          ? customFrameslength / 2 / fps // customFrameslength / 2 divided by fps
          : this.state.startTime,
      endTime:
        this.state.endTime === 0
          ? customFrameslength / 2 / fps + 1 // this +1  can be any number it shows that when user clicks on custom then initially gap is of 2 sec btw start & end selectors
          : this.state.endTime,
    });
  }

  fullVideoClicked() {
    this.setState({
      windowFrames: [],
      start: null,
      loopFullVideo: true,
      openPlaybackLoopControls: false,
      end: null,
      openCustomFrames: false,
      window1Start: 1 * fps, // *fps bcoz we have to pass index not the time inside state when classname condition is written
      window1End: 3 * fps - 1, // -1 bcoz it selects 1 extra frame at the end
      window2Start: 5 * fps,
      window2End: 8 * fps - 1,
    });
  }

  fetchCurrTime(e) {
    this.setState({
      currTimeFromLoopComponent: e,
    });
  }

  render() {
    const {
      openCustomFrames,
      framesLine,
      windowFrames,
      window1Start,
      window1End,
      window2Start,
      window2End,
      customFramesExtracted,
      start,
      end,
      videoDuration,
      openPlaybackLoopControls,
      frameByFrameView,
    } = this.state;
    var totalFrames = Math.round(this.state.videoDuration) * fps - 2; //  -2 for rendering main component when only last two frames are left to be extracted
    var videoWidth = 1280;
    var windowFramesLength = this.state.windowFrames.length;
    var normalFrameLength = this.state.framesLine.length;
    var customFrameLength = this.state.customFramesExtracted.length - 2; // -2 bcoz we have a selector line also inside the array whose width can't be same as of image
    var windowFrameImgSize = videoWidth / windowFramesLength;
    var normalFrameImgSize = videoWidth / normalFrameLength;
    var customFrameImgSize = videoWidth / customFrameLength;

    if (!this.state.openPlaybackLoopControls || this.state.openCustomFrames) {
      // for progress line in full video & custom playback options
      moveProgressLine =
        this.state.currTimeFromLoopComponent *
        (videoWidth / this.state.videoDuration);
    } else if (
      this.state.openPlaybackLoopControls &&
      !this.state.windowTwoClicked
    ) {
      // for progress line in window1 playback option
      var newVideoWidth =
        (videoWidth / lengthOfWinArray) * this.state.noOfWinFrames;
      var widthPerSec = newVideoWidth / this.state.noOfWinFrames;

      if (this.state.currTimeFromLoopComponent === this.state.startTime) {
        moveProgressLine = 0;
      } else if (
        Math.round(this.state.currTimeFromLoopComponent) ===
        this.state.endTime - 1
      ) {
        moveProgressLine =
          (this.state.currTimeFromLoopComponent - this.state.startTime + 1) *
          widthPerSec;
      } else if (
        Math.round(this.state.currTimeFromLoopComponent) <
        this.state.endTime - 1
      ) {
        moveProgressLine =
          (this.state.currTimeFromLoopComponent - this.state.startTime) *
          widthPerSec;
      }
    } else if (
      this.state.openPlaybackLoopControls &&
      this.state.windowTwoClicked
    ) {
      // for progress line in window2 playback option
      newVideoWidth =
        (videoWidth / lengthOfWinArray) * this.state.noOfWinFrames;
      widthPerSec = newVideoWidth / (this.state.endTime - this.state.startTime);

      moveProgressLine =
        (this.state.currTimeFromLoopComponent -
          this.state.startTime +
          this.state.prevNoOfWinFrames) *
        widthPerSec;
    }

    return (
      <>
        {this.state.framesExtracted.length == 0 ||
        this.state.framesExtracted.length < totalFrames ? (
          <>
            <div>
              <div style={{ position: "relative" }}>
                <video
                  width="1280"
                  height="720"
                  className="firstPlayer"
                  id="my-video"
                  crossOrigin="anonymous"
                  autoPlay
                  muted
                >
                  <source src={videoToShow} type="video/mp4" />
                  <source src="./sampleVideo.webm" type="video/webm" />
                </video>
                <div className="videoLoader">
                  <Loader />
                </div>
              </div>
            </div>
          </>
        ) : (
          <div>
            {this.state.frameByFrameView == false ? (
              !this.state.openPlaybackLoopControls ? (
                <VideoLooper
                  loopFullVideo={this.state.loopFullVideo}
                  fetchCurrTime={this.fetchCurrTime}
                />
              ) : (
                <VideoLooper
                  start={this.state.startTime}
                  end={this.state.endTime}
                  duration={this.state.videoDuration}
                  fetchCurrTime={this.fetchCurrTime}
                />
              )
            ) : (
              <img
                src={
                  this.state.framePreviewSrc !== undefined &&
                  this.state.framePreviewSrc.length == 0
                    ? this.state.framesExtracted[0]
                    : this.state.framePreviewSrc
                }
                style={{ width: "1280px", height: "720px" }}
                alt="imageofframes"
              />
            )}

            {/* frame bar div start */}

            <FrameBar
              openCustomFrames={openCustomFrames}
              framesLine={framesLine}
              windowFrames={windowFrames}
              window1Start={window1Start}
              window2Start={window2Start}
              window2End={window2End}
              window1End={window1End}
              moveProgressLine={moveProgressLine}
              normalFrameImgSize={normalFrameImgSize}
              windowFrameImgSize={windowFrameImgSize}
              customFramesExtracted={customFramesExtracted}
              start={start}
              end={end}
              customFrameImgSize={customFrameImgSize}
              showPreviewOnClick={this.showPreviewOnClick}
              onDragEnd={this.onDragEnd}
            />

            {/* frame bar div ends */}

            {/* playback controls div starts */}

            <PlaybackControls
              fullVideoClicked={this.fullVideoClicked}
              passPropsToVideoLooperWinOne={this.passPropsToVideoLooperWinOne}
              passPropsToVideoLooperWinTwo={this.passPropsToVideoLooperWinTwo}
              customButtonOnClick={this.customButtonOnClick}
              frameByFrameView={frameByFrameView}
              frameByFrameViewMethod={this.frameByFrameView}
              sliderChange={this.sliderChange}
              skipVideo={this.skipVideo}
              videoStatus={this.state.videoStatus}
              pauseVideo={this.pauseVideo}
              playVideo={this.playVideo}
              skipVideo={this.skipVideo}
              showForBackButton={this.state.showForBackButton}
              framePreviewForward={this.framePreviewForward}
              framePreviewBackward={this.framePreviewBackward}
            />

            {/* playback controls div ends */}
          </div>
        )}
      </>
    );
  }
}
