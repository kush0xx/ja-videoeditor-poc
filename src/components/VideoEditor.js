import React, { Component } from "react";
import videoConnect from "react-html5video";
import "react-html5video/dist/styles.css";
import videoToShow from "./sampleVideo.mp4";
import "./videoStyle.scss";
import { Player, ControlBar } from "video-react";
import "react-video-trimmer/dist/style.css";

// const videoToShow =
//   "https://drive.google.com/file/d/1LO8VgkC-L4UItiYvQ8c9Kg4nM3AlPxRj/view";

var totalVideoTime;
class VideoEditor extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      source: videoToShow,
      startTime: 0,
      playPause: false,
      endTime: 0,
      currentTime: 0,
      zoom: 1,
      videoDuration: 0,
      scrubTime: 0,
      scrubTimeRight: 0,
      start: 0,
      end: totalVideoTime,
      prevStartState: 0,
      prevEndState: 0,
      sliderLeftProperty: 0,
      openLoopComponent: false,
      image: null,
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.scrub = this.scrub.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.playPause = this.playPause.bind(this);
  }

  // componentDidUpdate() {
  //   // this will scroll the loop componet to top showing the video as soon as the component loads which will stop flickering while switching component
  //   if (document.querySelector(".loopcomponent")) {
  //     $(document).ready(function () {
  //       // Handler for .ready() called.
  //       $("html, body").animate(
  //         {
  //           scrollTop: $("#scroll").offset().top,
  //         },
  //         1
  //       );
  //     });
  //   }
  // }

  componentDidMount() {
    // subscribe state change
    this.player.subscribeToStateChange(this.handleStateChange.bind(this));
    const player = document.querySelector(".player");
    const progress = player.querySelector(".progress");
    const realPlayer = player.querySelector(".video-react-video");

    let mousedown = false;
    progress.addEventListener("click", this.scrub);
    progress.addEventListener("mousemove", (e) => mousedown && this.scrub(e));

    realPlayer.addEventListener("click", (e) => this.playPause(e));
  }

  playPause(e) {
    this.setState({
      playPause: !this.state.playPause,
    });
  }

  // copy player state to this component's state

  handleStateChange(state) {
    this.setState({
      player: state,
      currentTime: state.currentTime,
      videoDuration: state.duration,
      endTime: state.duration,
    });

    totalVideoTime = this.state.duration;
    const player = document.querySelector(".player");
    const progressBar = player.querySelector(".progress__filled");
    const percent = (state.currentTime / state.duration) * 100;
    progressBar.style.flexBasis = `${percent}%`;
  }

  // this method take the seek time i.e the time when user click on progress bar and sestate(scrubTime) and immediately calls the seek method
  scrub(e) {
    const player = document.querySelector(".player");
    const progress = player.querySelector(".progress");

    const scrubTime =
      (e.offsetX / progress.offsetWidth) * this.state.videoDuration;
    this.setState(
      {
        scrubTime: scrubTime,
      },
      () => this.seek()
    );
  }

  // it skips the video to the seek time i.e time when user click on progress bar
  seek() {
    this.player.seek(this.state.scrubTime);

    this.setState((prevState) => {
      return {
        prevStartState: prevState.start,
      };
    });

    this.setState((prevState) => {
      return {
        prevEndState: prevState.end,
      };
    });
  }

  play() {
    this.player.play();
    this.setState({
      playPause: true,
    });
  }

  pause() {
    this.player.pause();
    this.setState({
      playPause: false,
    });
  }

  changePlaybackRateRate(e) {
    this.player.playbackRate = e.target.value;
  }

  zoomIn() {
    this.setState((prevState) => {
      return { zoom: prevState.zoom + 0.1 };
    }, this.zoomInFunc);
  }

  zoomInFunc() {
    var properties = [
        "transform",
        "WebkitTransform",
        "MozTransform",
        "msTransform",
        "OTransform",
      ],
      prop = properties[0];
    if (document.getElementById("video")) {
      var vid = document.getElementById("video");
      vid.style.left = 0;
      vid.style.top = 0;
      var rotate = 0;
      vid.style[prop] =
        "scale(" + this.state.zoom + ") rotate(" + rotate + "deg)";
    }
  }

  zoomOut() {
    this.setState((prevState) => {
      return { zoom: prevState.zoom - 0.1 };
    }, this.zoomOutFunc);
  }

  zoomOutFunc() {
    var properties = [
        "transform",
        "WebkitTransform",
        "MozTransform",
        "msTransform",
        "OTransform",
      ],
      prop = properties[0];
    if (document.getElementById("video")) {
      var vid = document.getElementById("video");
      vid.style.left = 0;
      vid.style.top = 0;

      var rotate = 0;
      vid.style[prop] =
        "scale(" + this.state.zoom + ") rotate(" + rotate + "deg)";
    }
  }

  render() {
    const timestamps = ["13", "35", "70", "50"];
    const items = [];
    timestamps.map((time) => {
      items.push(
        <div className="dots" style={{ left: `${time}%` }}>
          <div className="work123 count-no" title="Toggle Play">
            1
          </div>
        </div>
      );
    });

    return (
      <div>
        <div className="player">
          {/* video player */}

          <div id="video" className="realPlayer">
            <Player
              ref={(player) => {
                this.player = player;
              }}
              // autoPlay={true}
              startTime={this.state.start}
              className="player__video viewer"
              controls={false}
              muted={true}
              loop={this.props.loopFullVideo}
            >
              <ControlBar autoHide={false} />
              {/* {console.log(`${videoToShow}#t=${this.state.start},${this.state.end}`,"nnnn")} */}
              <source
                // src={`${videoToShow}#t=20,25`} //we can pass custom start & end time like this
                // src={`${videoToShow}#t=${this.state.start},${this.state.end}`}
                src={videoToShow}
              />
            </Player>
          </div>

          {/* video player ends */}

          {/* player controls */}

          {
            <div className="player__controls">
              <div className="progress">
                <div className="progress__filled"></div>
              </div>

              {items}

              <div className="buttons-wrapper">
                <div className="left-btn-wrapper">
                  <button
                    className={
                      this.state.playPause
                        ? "player__button toggle"
                        : "player__button toggle hide"
                    }
                    title="Toggle Play"
                    onClick={this.pause}
                  >
                    <i class="fa fa-pause" aria-hidden="true"></i>
                  </button>

                  <button
                    className={
                      this.state.playPause == false
                        ? "player__button toggle"
                        : "player__button toggle hide"
                    }
                    title="Toggle Play"
                    onClick={this.play}
                  >
                    <i class="fa fa-play" aria-hidden="true"></i>
                  </button>

                  <select id="comboA" onChange={this.changePlaybackRateRate}>
                    <option value="0.25">0.25</option>
                    <option value="0.5">0.5</option>
                    <option value="0.75">0.75</option>
                    <option value="1" selected>
                      Normal
                    </option>
                    <option value="1.5">1.25</option>
                    <option value="1.5">1.5</option>
                    <option value="1.5">1.75</option>
                    <option value="2">2</option>
                  </select>
                </div>
                <div className="right-btn-wrapper zoom-in-btn-wrapper">
                  <button className="getTime" onClick="getCurrentTime()">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                  </button>

                  <button className="player__button" onClick={this.zoomIn}>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </button>
                  <button className="player__button" onClick={this.zoomOut}>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            </div>
          }

          {/* player controls ends */}
        </div>
      </div>
    );
  }
}

export default videoConnect(VideoEditor);
