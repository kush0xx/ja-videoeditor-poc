import React from "react";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import "./video.css";

const marks = {
  0: "1/10",
  20: "1/5",
  40: "1/4",
  60: "1/3",
  80: "1/2",
  100: <strong>1</strong>,
};

function PlaybackControls(props) {
  return (
    <div className="playbacktimewindow">
      <div>
        {!props.frameByFrameView ? (
          <div className="main-container">
            <div className="playback-window-controls">
              PlayBack Time Window
              <div>
                <button onClick={() => props.fullVideoClicked()}>
                  Full Video
                </button>

                <button
                  onClick={() => props.passPropsToVideoLooperWinOne(1, 3)}
                >
                  Window1
                </button>
                <button
                  onClick={() => props.passPropsToVideoLooperWinTwo(5, 8)}
                >
                  Window2
                </button>
                <button
                  id="customBtn"
                  onClick={() => props.customButtonOnClick()}
                >
                  Custom
                </button>
              </div>
            </div>

            <div className="playPauseSkip">
              Playback Controls
              <div>
                <button onClick={() => props.skipVideo(-10)}>-10</button>
                <button
                  className={props.videoStatus == "play" ? "" : "hide"}
                  title="Toggle Play"
                  onClick={() => props.pauseVideo()}
                >
                  <i className="fa fa-pause" aria-hidden="true"></i>
                </button>
                <button
                  className={
                    props.videoStatus == "pause" || props.videoStatus == ""
                      ? ""
                      : "hide"
                  }
                  title="Toggle Play"
                  onClick={() => props.playVideo()}
                >
                  <i className="fa fa-play" aria-hidden="true"></i>
                </button>
                <button onClick={() => props.skipVideo(10)}>+10</button>
              </div>
            </div>
            <div className="playbackSpeed">
              Playback Speed
              <Slider
                defaultValue={100}
                step={20}
                min={0}
                marks={marks}
                dots
                onChange={(val) => props.sliderChange(val)}
              />
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
      {/* frame by frame view starts*/}
      <div className="frame-by-frame">
        <div>
          <button onClick={() => props.frameByFrameViewMethod()}>
            {props.frameByFrameView ? "Back" : "Frame By Frame"}
          </button>
        </div>
        <div>
          {props.showForBackButton && props.frameByFrameView ? (
            <div>
              <button onClick={() => props.framePreviewForward()}>Forward</button>
              <button onClick={() => props.framePreviewBackward()}>Backward</button>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
      {/* frame by frame view ends */}
    </div>
  );
}

export default PlaybackControls;
