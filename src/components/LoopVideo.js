import React, { Component } from "react";
import videoToShow from "./sampleVideo.mp4";
import "./video.css";
export default class LoopVideo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currVideoTime: 0,
      duration: 0,
      videoStatus: "",
    };
    this.checkCurrTime = this.checkCurrTime.bind(this);
  }

  componentDidMount() {
    const video = document.getElementById("my-video");
    video.addEventListener("timeupdate", this.checkCurrTime);
  }

  checkCurrTime() {
    if (!this.props.loopFullVideo && document.getElementById("my-video")) {
      const video = document.getElementById("my-video");
      this.setState({
        currVideoTime: Math.round(video.currentTime),
        duration: video.duration,
      });
    }
    if (document.getElementById("my-video")) {
      const video = document.getElementById("my-video");
      this.props.fetchCurrTime(video.currentTime);
    }
  }

  componentDidUpdate(prevProps) {
    if (!this.props.loopFullVideo) {
      const video = document.getElementById("my-video");

      if (prevProps.start !== this.props.start) {
        video.currentTime = this.props.start;
        var playPromise = video.play();

        if (playPromise !== undefined) {
          playPromise
            .then((_) => {
              // Automatic playback started!
              // Show playing UI.
              console.log("audio played auto");
            })
            .catch((error) => {
              // Auto-play was prevented
              // Show paused UI.
              console.log("playback prevented");
            });
        }
      }

      if (this.state.currVideoTime >= this.props.end) {
        video.currentTime = this.props.start;
        video.play();
      }
    }
  }

  render() {
    return (
      <>
        <div>
          <video
            width="1280"
            height="720"
            id="my-video"
            crossOrigin="anonymous"
            // autoPlay
            // controls
            muted
            loop={this.props.loopFullVideo ? true : false}
          >
            <source
              src={
                this.props.loopFullVideo
                  ? videoToShow
                  : `${videoToShow}#t=${this.props.start},${
                      this.props.end == 0 ? this.state.duration : this.props.end
                    }`
              }
              type="video/mp4"
            />
          </video>
        </div>

        {/* middle controls */}
      </>
    );
  }
}
